package appnomic

// DiffInterface will be used to find the diff. Then the CREATE and DELETE requests
// to appnomic will be sent to appnomic based on this diff. It has a function
// GetDiffIds which returns a map of string to bool. bool value here signifies the
// operation needed. For new objects creation the value will be true. for deletion the
// value will be false.
type DiffInterface interface {
	GetDiffIds(n *Topology) map[string]bool
	GetDiffObjects(nodeType string, n *Topology) ([]string, []string)
}

var gDiffObj DiffInterface

// LocalDiff implements DiffInterface. LocalDiff is the stateful version of the
// appnomic collector. it stores the last diff in the variable gOldTopology. On
// completion of every successive topology creation cycle the topology is stored in
// gOldTopology.
type LocalDiff struct {
}

// Go through the ids of the new topology. The added ones will not be present in the
// old topology. For these IDs mark true in the diffIds map.
// Go through the ids of the old topology. The removed ids will be present in the old
// but not in new. For these IDs mark false in the diffIds map.
func (l LocalDiff) GetDiffIds(n *Topology) map[string]bool {
	diffIds := make(map[string]bool)
	for id := range n.nodeSummaries {
		if !gOldTopology.Exists(id) {
			diffIds[id] = true
		}
	}
	for id := range gOldTopology.nodeSummaries {
		if !n.Exists(id) {
			diffIds[id] = false
		}
	}
	return diffIds
}


// Go through the ids of the new topology. The added ones will not be present in the
// old topology. For these IDs mark true in the diffIds map.
// Go through the ids of the old topology. The removed ids will be present in the old
// but not in new. For these IDs mark false in the diffIds map.
// Change this to return slice of objects and the operation like create/delete/update
// by comparing with appn inventory one read apis are available..
func (l LocalDiff) GetDiffObjects(nodeType string, n *Topology) ([]string, []string) {
	added := make([]string, 0)
	for id, node := range n.nodeSummaries {
		if node.nodeType == nodeType {
			if !gOldTopology.Exists(id) {
				added = append(added, id)
			}
		}
	}

	removed := make([]string, 0)
	for id, node := range gOldTopology.nodeSummaries {
		if node.nodeType == nodeType {
			if !n.Exists(id) {
				removed = append(removed, id)
			}
		}
	}

	return added, removed
}
