package appnomic

import (
	"encoding/json"
	"errors"
	"log"
)

type Tag struct {
	Identifier  string `json:"identifier"`
	SubTypeName string `json:"subTypeName"`
	Name        string `json:"name"`
	Layer       string `json:"layer,omitempty"`
}

type Application struct {
	Identifier          string      `json:"identifier"`
	Name                string      `json:"name"`
	TxnViolationConfigs interface{} `json:"txnViolationConfigs"`
	Tags                []Tag       `json:"tags"`
}

type Cluster struct {
	Identifier          string      `json:"identifier"`
	MstComponentName    string      `json:"mstComponentName"`
	Discovery           int         `json:"discovery"`
	Name                string      `json:"name"`
	MstComponentVersion string      `json:"mstComponentVersion"`
	MstComponentType    string      `json:"mstComponentType"`
	MappingIdentifiers  []string    `json:"mappingIdentifiers,omitempty"`
	Kpi                 interface{} `json:"kpi"`
	Tags                []Tag       `json:"tags"`
}

type Attribute struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type CompInstance struct {
	Name                string      `json:"name"`
	Identifier          string      `json:"identifier"`
	ClusterName         string      `json:"clusterName"`
	MstComponentVersion string      `json:"mstComponentVersion"`
	MstComponentName    string      `json:"mstComponentName"`
	MstComponentType    string      `json:"mstComponentType"`
	MappingIdentifiers  []string    `json:"mappingIdentifiers,omitempty"`
	Discovery           int         `json:"discovery"`
	Tags                []Tag       `json:"tags"`
	Attributes          []Attribute `json:"attributes"`
	GroupKpi            interface{} `json:"groupKpi"`
}

type ComponentInfo struct {
	Name      string
	Version   string
	Type      string
	Discovery int
}

var componentInfo map[string]ComponentInfo = map[string]ComponentInfo{

	/*
	   "golang":     ComponentInfo{Name: "GO Container", Version: "1.13", Type: "Services", Discovery: 1},
	   "java":   ComponentInfo{Name: "JAVA Container", Version: "11.0.5+10", Type: "Services", Discovery: 1},
	   "nginx":  ComponentInfo{Name: "NGINX Container", Version: "1.17.7", Type: "Services", Discovery: 1},
	   "nodejs":   ComponentInfo{Name: "Node JS Container", Version: "13.7.0", Type: "Services", Discovery: 1},
	   "python": ComponentInfo{Name: "Python Container", Version: "3.8.1", Type: "Services", Discovery: 1},
	   "unknown":  ComponentInfo{Name: "Container", Version: "1.13", Type: "Services", Discovery: 1},
	*/

	"Pod":     ComponentInfo{Name: "Pod", Version: "1.13", Type: "Host", Discovery: 1},
	"golang":  ComponentInfo{Name: "GO Container", Version: "1.13", Type: "Services", Discovery: 1},
	"java":    ComponentInfo{Name: "JAVA Container", Version: "11.0.5+10", Type: "Services", Discovery: 1},
	"nginx":   ComponentInfo{Name: "NGINX Container", Version: "1.17.7", Type: "Services", Discovery: 1},
	"nodejs":  ComponentInfo{Name: "Node JS Container", Version: "13.7.0", Type: "Services", Discovery: 1},
	"python":  ComponentInfo{Name: "Python Container", Version: "3.8.1", Type: "Services", Discovery: 1},
	"unknown": ComponentInfo{Name: "Container", Version: "1.13", Type: "Services", Discovery: 1},
}

/*
var containerInfo map[string]string = map[string]string{
	"go-hello-world": "GO",
	"java-hello-world": "JAVA",
	"nginx-hello-world":  "NGINX",
	"node-hello-world":   "NODE",
	"python-hello-world": "PYTHON",
}
*/

type Connection struct {
	SourceServiceName      string `json:"sourceServiceName"`
	DestinationServiceName string `json:"destinationServiceName"`
	IsDiscovery            int    `json:"isDiscovery"`
}

func createConnection(topo *Topology) error {
	var connections []Connection
	for _, node := range topo.nodeSummaries {
		if node.nodeType == "service" {
			log.Printf("")
			for _, adjacent_id := range node.nodeSummary.Adjacency {
				neighbour, ok := topo.nodeSummaries[adjacent_id]
				if !ok {
					log.Printf("Could Not find Neighbour with node id %s for Source id %s", adjacent_id, node.label)
					continue
				}
				connections = append(connections, Connection{SourceServiceName: node.label, DestinationServiceName: neighbour.label, IsDiscovery: 1})
			}
		}
	}
	if len(connections) == 0 {
		log.Printf("No Connections")
		return nil
	}
	json, err := json.Marshal(connections)
	if err != nil {
		log.Printf("error in unmarshal connection json %s", err.Error())
		return err
	}

	log.Printf("connection json %s", string(json))
	respBody, status := gExporter.SendToAppn("connection", string(json))
	if status == 200 {
		log.Printf("AddConnection: Response Success")
	} else {
		log.Printf("AddConnection Failed with Response:%d \n Body:%s", status, string(respBody))
	}
	return nil
}

func createApplication(appid, appname string, added []string, topo *Topology) error {
	if len(added) == 0 {
		log.Printf("no new services to add")
		err := createConnection(topo)
		if err != nil {
			log.Println("Failure in Connection Creation")
			return err
		}
		return nil
	}

	app := Application{
		Identifier:          appid,
		Name:                appname,
		TxnViolationConfigs: nil,
		Tags:                make([]Tag, 0),
	}

	for _, nodeid := range added {
		node, ok := topo.nodeSummaries[nodeid]
		if !ok {
			log.Printf("node id %s not in topology. it's a bug, please fix", nodeid)
			return errors.New("node not found in topology while adding")
		}

		servicetag := Tag{
			SubTypeName: "Services",
			Layer:       "app_service",
			Name:        "Controller",
			Identifier:  node.label,
		}
		app.Tags = append(app.Tags, servicetag)
	}

	// add timezone tag
	timezonetag := Tag{
		SubTypeName: "Account",
		Name:        "Timezone",
		Identifier:  "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi",
	}

	app.Tags = append(app.Tags, timezonetag)

	apps := []Application{app}
	json, err := json.Marshal(apps)
	if err != nil {
		log.Printf("error in unmarshal application json %s", err.Error())
		return err
	}

	log.Printf("application json %s", string(json))
	respBody, status := gExporter.SendToAppn("service", string(json))
	if status == 200 {
		log.Printf("AddApplication: Response Success")
	} else {
		log.Printf("AddApplication Failed with Response:%d \n Body:%s", status, string(respBody))
	}
	for _, nodeid := range added {
		node, ok := topo.nodeSummaries[nodeid]

		if !ok {
			log.Printf("node id %s not in topology. it's a bug, please fix", nodeid)
			return errors.New("node not found in topology while adding")
		}

		//get a pod node
		for _, pod_node := range node.children {
			log.Println("#################################")
			log.Println("#################################")
			log.Println("PodNode %s", pod_node.label)
			var pod_root_label string
			for _, parent := range pod_node.nodeSummary.Parents {
				if parent.TopologyID == "kube-controllers" {
					log.Println("Creating Pod Cluster %s for service %s", parent.Label, node.label)
					pod_root_label = parent.Label
					err = createPodCluster(parent.Label, node.label, appid)
					if err != nil {
						log.Println("error in creating pod cluster for newly added service %s %s", node.label, err.Error())
						return err
					}
				}
				break
			}

			container_created := false

			// create Container Cluster for every child of pod
			for _, container_node := range pod_node.children {
				log.Println("Creating Container Cluster %s for service %s", container_node.label, node.label)

				platform := getPlatform(pod_node)
				err = createContainerCluster(pod_root_label, container_node.label, node.label, appid, platform)

				if err != nil {
					log.Println("error in creating container cluster for newly added service %s %s", node.label, err.Error())
					return err
				}
				container_created = true
			}
			if container_created {
				break
			} else {
				log.Println("No Containers found")
				continue
			}
		}

		/*
			err = createPodCluster(node.label, appid)
			if err != nil {
				log.Println("error in creating pod cluster for newly added service %s %s", node.label, err.Error())
				return err
			}
			err = createContainerCluster(node.label, appid)
			if err != nil {
				log.Println("error in creating container cluster for newly added service %s %s", node.label, err.Error())
				return err
			}
		*/
	}

	err = createConnection(topo)
	if err != nil {
		log.Println("Failure in Connection Creation")
		return err
	}
	return nil
}

func getPlatform(pod_node *Node) string {
	for _, meta := range pod_node.nodeSummary.Metadata {
		if meta.ID == "kubernetes_appn_platform" {
			return meta.Value
		}
	}
	return "unknown"
}

func createContainerCluster(pod_root_name, container_name, serviceid, appid, platform string) error {
	clustername := "CONTAINER_Cluster-" + pod_root_name + "-" + container_name
	clusterid := clustername
	mappingidentifiers := []string{"POD_Cluster-" + pod_root_name}
	/*
		for k,_ := range componentInfo {
			if strings.Contains(serviceid, strings.ToLower(k)) {
				componentname = k
			}
		}
	*/
	componentname := platform

	return createCluster(clusterid, clustername, componentname, serviceid, appid, mappingidentifiers)
}

func createPodCluster(pod_root_name, serviceid, appid string) error {
	clustername := "POD_Cluster-" + pod_root_name
	clusterid := clustername
	return createCluster(clusterid, clustername, "Pod", serviceid, appid, []string{})
}

func createCluster(clusterid, clustername, compname, serviceid, appid string, mappingidentifiers []string) error {
	cluster := Cluster{
		Identifier:          clusterid,
		Name:                clustername,
		MstComponentName:    componentInfo[compname].Name,
		Discovery:           componentInfo[compname].Discovery,
		MstComponentVersion: componentInfo[compname].Version,
		MstComponentType:    componentInfo[compname].Type,
		MappingIdentifiers:  mappingidentifiers,
		Kpi:                 nil,
		Tags:                make([]Tag, 0),
	}

	// add service tag
	servicetag := Tag{
		SubTypeName: "Services",
		Layer:       "app_service",
		Name:        "Controller",
		Identifier:  serviceid,
	}
	cluster.Tags = append(cluster.Tags, servicetag)

	// add app tag
	apptag := Tag{
		SubTypeName: "Application",
		Name:        "Controller",
		Identifier:  appid,
	}
	cluster.Tags = append(cluster.Tags, apptag)

	clusters := []Cluster{cluster}
	json, err := json.Marshal(clusters)
	if err != nil {
		log.Printf("error in unmarshal cluster json %s", err.Error())
		return err
	}
	log.Printf("cluster json %s", string(json))
	respBody, status := gExporter.SendToAppn("cluster", string(json))
	if status == 200 {
		log.Printf("AddCluster: Response Success")
	} else {
		log.Printf("AddCluster Failed with Response:%d \n Body:%s", status, string(respBody))
	}
	return nil
}

func createPodCompInstances(appid, appname string, added []string, topo *Topology) error {
	for _, nodeid := range added {
		node, ok := topo.nodeSummaries[nodeid]
		if !ok {
			log.Printf("node id %s not in topology. it's a bug, please fix", nodeid)
			return errors.New("node not found in topology while adding")
		}

		err := createPodCompInstance(appid, appname, node)
		if err != nil {
			log.Println("error in creating pod comp instance for newly added pod %s %s", node.label, err.Error())
			return err
		}
	}

	return nil
}

func createContainerCompInstances(appid, appname string, added []string, topo *Topology) error {
	for _, nodeid := range added {
		node, ok := topo.nodeSummaries[nodeid]
		if !ok {
			log.Printf("node id %s not in topology. it's a bug, please fix", nodeid)
			return errors.New("node not found in topology while adding")
		}

		err := createContainerCompInstance(appid, appname, node)
		if err != nil {
			log.Println("error in creating pod comp instance for newly added pod %s %s", node.label, err.Error())
			return err
		}
	}

	return nil
}

func createContainerCompInstance(appid, appname string, node *Node) error {
	var podnode *Node
	for _, data := range node.parents {
		if data.nodeType == pod {
			//podid = data.id
			podnode = data
			break
		}
	}

	var servicenode *Node
	for _, data := range podnode.parents {
		if data.nodeType == service {
			servicenode = data
			break
		}
	}

	var attributes []Attribute
	for _, data := range podnode.nodeSummary.Metadata {
		id := data.ID
		switch id {
		case "kubernetes_ip":
			attributes = append(attributes, Attribute{Name: "HostAddress", Value: data.Value})
		}
	}

	instanceid := podnode.label + "_" + node.label
	instancename := instanceid

	for _, data := range node.nodeSummary.Metadata {
		id := data.ID
		switch id {
		case "docker_image_tag":
			attributes = append(attributes, Attribute{Name: "Image Tag", Value: data.Value})
		case "docker_image_name":
			attributes = append(attributes, Attribute{Name: "Image Name", Value: data.Value})
		case "docker_container_command":
			attributes = append(attributes, Attribute{Name: "Container Command", Value: data.Value})
		case "docker_container_state_human":
			attributes = append(attributes, Attribute{Name: "State", Value: data.Value})
		case "docker_container_restart_count":
			attributes = append(attributes, Attribute{Name: "Restart Count", Value: data.Value})
		case "docker_container_ips":
			attributes = append(attributes, Attribute{Name: "Container IPs", Value: data.Value})
		case "docker_container_created":
			attributes = append(attributes, Attribute{Name: "Created", Value: data.Value})
		case "docker_container_id":
			attributes = append(attributes, Attribute{Name: "Container ID", Value: data.Value})
		}
	}
	attributes = append(attributes, Attribute{Name: "Name", Value: instancename})
	attributes = append(attributes, Attribute{Name: "Uuid", Value: node.nodeSummary.ID})
	/*
		attributes = append(attributes, Attribute{Name: "Name", Value: node.nodeSummary.Label })
		attributes = append(attributes, Attribute{Name: "Uuid", Value: node.nodeSummary.ID })

		instanceid := podnode.label + "_" + node.label
		instancename := instanceid
	*/
	//clustername := "CONTAINER_Cluster-" + servicenode.label
	//fetch pod root label
	var pod_root_label string
	for _, parent := range podnode.nodeSummary.Parents {
		if parent.TopologyID == "kube-controllers" {
			pod_root_label = parent.Label
			break
		}
	}
	clustername := "CONTAINER_Cluster-" + pod_root_label + "-" + node.label
	mappingidentifiers := []string{podnode.label}
	componentname := getPlatform(podnode)

	return createCompInstance(instanceid, instancename, componentname, clustername, servicenode.label, appid, mappingidentifiers, attributes)
}

func createPodCompInstance(appid, appname string, node *Node) error {
	var serviceid string
	var pod_root_label string
	for _, data := range node.nodeSummary.Parents {
		if data.TopologyID == "services" {
			//serviceid = data.ID
			serviceid = data.Label
		}
		if data.TopologyID == "kube-controllers" {
			pod_root_label = data.Label
		}
	}
	clustername := "POD_Cluster-" + pod_root_label

	var attributes []Attribute
	for _, data := range node.nodeSummary.Metadata {
		id := data.ID
		switch id {
		case "kubernetes_state":
			attributes = append(attributes, Attribute{Name: "State", Value: data.Value})
		case "kubernetes_ip":
			attributes = append(attributes, Attribute{Name: "HostAddress", Value: data.Value})
		case "container":
			attributes = append(attributes, Attribute{Name: "Containers Count", Value: data.Value})
		case "kubernetes_namespace":
			attributes = append(attributes, Attribute{Name: "Namespace", Value: data.Value})
		case "kubernetes_restart_count":
			attributes = append(attributes, Attribute{Name: "Restart Count", Value: data.Value})
		case "kubernetes_created":
			attributes = append(attributes, Attribute{Name: "Created", Value: data.Value})
		}
	}
	attributes = append(attributes, Attribute{Name: "Name", Value: node.nodeSummary.Label})
	attributes = append(attributes, Attribute{Name: "Uuid", Value: node.nodeSummary.ID})

	return createCompInstance(node.nodeSummary.Label, node.nodeSummary.Label, "Pod", clustername, serviceid, appid, []string{}, attributes)
}

func createCompInstance(instanceid, instancename, compname, clustername, serviceid, appid string, mappingidentifiers []string, attributes []Attribute) error {
	compinstance := CompInstance{
		Identifier:          instanceid,
		Name:                instancename,
		ClusterName:         clustername,
		MstComponentName:    componentInfo[compname].Name,
		Discovery:           componentInfo[compname].Discovery,
		MstComponentVersion: componentInfo[compname].Version,
		MstComponentType:    componentInfo[compname].Type,
		MappingIdentifiers:  mappingidentifiers,
		Tags:                make([]Tag, 0),
		Attributes:          attributes,
	}

	// add service tag
	servicetag := Tag{
		SubTypeName: "Services",
		Layer:       "app_service",
		Name:        "Controller",
		Identifier:  serviceid,
	}
	compinstance.Tags = append(compinstance.Tags, servicetag)

	// add app tag
	apptag := Tag{
		SubTypeName: "Application",
		Name:        "Controller",
		Identifier:  appid,
	}
	compinstance.Tags = append(compinstance.Tags, apptag)

	compinstances := []CompInstance{compinstance}
	json, err := json.Marshal(compinstances)
	if err != nil {
		log.Printf("error in unmarshal compinstance json %s", err.Error())
		return err
	}
	log.Printf("compinstance json %s", string(json))
	respBody, status := gExporter.SendToAppn("compInstance", string(json))
	if status == 200 {
		log.Printf("AddCompInstance: Response Success")
	} else {
		log.Printf("AddCompInstance Failed with Response:%d \n Body:%s", status, string(respBody))
	}
	return nil
}
