package appnomic

import (
	"log"
	"time"

	"github.com/weaveworks/scope/report"
)

// Writer interface is used to write a node to the appnomic server.
// currently we will be using the json struct but later on we will migrate
// to the templates to write. So that's why an interface will be handy and
// when we will have the template logic, we will plug in the new interface
// to that we wont end up removing and code and switch back to the older working
// by switching it to the older writer.
type Writer interface {
	Write(node *Node, operation bool) error
	WriteObjects(nodeType string, added, removed []string, topo *Topology) error
}

var gWriter Writer

type LocalRemoteWriter struct {
}

const (
	container = "container"
	pod       = "pod"
	service   = "service"
	host      = "host"
)

const (
	blackxappname = "Blackx"
	blackxappid   = "Blackx"
)


func (l LocalRemoteWriter) WriteObjects(nodeType string, added, removed []string, topo *Topology) error {
	switch nodeType {
	case service:
		err := createApplication(blackxappid, blackxappname, added, topo)
		if err != nil {
			log.Println("error in creating application with newly added services %s", err.Error())
			return err
		}

		for _, nodeid := range removed {
			log.Printf("service node id %s removed from topology. integrate once appn api is available", nodeid)
		}

	case pod:
		err := createPodCompInstances(blackxappid, blackxappname, added, topo)
		if err != nil {
			log.Println("error in creating comp instances with newly added pods %s", err.Error())
			return err
		}

		for _, nodeid := range removed {
			log.Printf("pod node id %s removed from topology. integrate once appn api is available", nodeid)
		}

	case container:
		err := createContainerCompInstances(blackxappid, blackxappname, added, topo)
		if err != nil {
			log.Println("error in creating comp instances with newly added containers %s", err.Error())
			return err
		}

		for _, nodeid := range removed {
			log.Printf("container node id %s removed from topology. integrate once appn api is available", nodeid)
		}

	default:
	}

	return nil
}

func (l LocalRemoteWriter) Write(n *Node, operation bool) error {
	if operation == true {
		switch n.nodeType {
		case service:
			lParsedResp := parseServiceMetrics(n)
			if lParsedResp == nil {
				log.Println("respnse empty")
			}
			// json creation logic needs to be added here.
			// it should be something like, from the parsed response
			// create the json and send it to the appnomic server.
			// sendToAppnomic() function can be modified to make it work

		case pod:
			lParsedResp := parsePodMetrics(n)
			if lParsedResp == nil {
				log.Println("respnse empty")
			}
			// json creation logic needs to be added here.
			// it should be something like, from the parsed response
			// create the json and send it to the appnomic server.

		case container:
			lParsedResp := parseContainerMetrics(n)
			if lParsedResp == nil {
				log.Println("respnse empty")
			}
			// json creation logic needs to be added here.
			// it should be something like, from the parsed response
			// create the json and send it to the appnomic server.

		default:
		}
	} else {
		log.Printf("Deleting %s, this is a log only and there will not be any deletion..", n.id)
	}
	return nil
}

// struct used to store host response. This will be used for json body creation
// to send to appnomic
type hostResp struct {
	id, name, timestamp string
	connections         report.IDList
	mem_usage_max       float64
	mem_usage_min       float64
	mem_usage_current   float64
	cpu_usage_max       float64
	cpu_usage_min       float64
	cpu_usage_current   float64
	local_networks      string
	uptime              string
	kernel_version      string
	os                  string
}

// struct used to store service response. This will be used for json body creation
// to send to appnomic
type serviceResp struct {
	id, name, timestamp string
	connections         report.IDList
	ports               string
	namespace           string
	pods_count          string
}

// struct used to store pod response. This will be used for json body creation
// to send to appnomic
type podResp struct {
	id, uuid, name, timestamp string
	service_id                string
	connections               report.IDList
	host_id                   string
	mem_usage_max             float64
	mem_usage_min             float64
	mem_usage_current         float64
	cpu_usage_max             float64
	cpu_usage_min             float64
	cpu_usage_current         float64
	restart_count             string
	Namespace                 string
	Containers_count          string
	HostAddress               string
	State                     string
}

// struct used to store container response. This will be used for json body creation
// to send to appnomic
type containerResp struct {
	id, uuid, name, timestamp string
	host_id                   string
	pod_name                  string
	pod_id                    string
	connections               report.IDList
	mem_usage_max             float64
	mem_usage_min             float64
	mem_usage_current         float64
	cpu_usage_max             float64
	cpu_usage_min             float64
	cpu_usage_current         float64
	created                   string
	ip_addr                   string
	restart_count             string
	container_uptime          string
	container_command_count   string
	image_name                string
	image_tag                 string
}

// struct used to store processResp response. This will be used for json body creation
// to send to appnomic
type processResp struct {
	id, uuid, name, timestamp string
	connections               report.IDList
	mem_usage_max             float64
	mem_usage_min             float64
	mem_usage_current         float64
	cpu_usage_max             float64
	cpu_usage_min             float64
	cpu_usage_current         float64
	pid                       string
	threads_count             string
	container_id              string
	host_id                   string
}

// parses the node to hostResp
func parseHostMetrics(n *Node) *hostResp {
	lHostResp := &hostResp{
		id:        n.id,
		name:      n.label,
		timestamp: time.Now().UTC().String(),
	}
	for _, data := range n.nodeSummary.Metadata {
		switch data.ID {
		case "os":
			lHostResp.os = data.Value
		case "kernel_version":
			lHostResp.kernel_version = data.Value
		case "uptime":
			lHostResp.uptime = data.Value
		case "local_networks":
			lHostResp.local_networks = data.Value
		}
	}
	for _, data := range n.nodeSummary.Metrics {
		id := data.ID
		if id == "host_cpu_usage_percent" {
			lHostResp.cpu_usage_current = data.Value
			lHostResp.cpu_usage_min = data.Metric.Min
			lHostResp.cpu_usage_max = data.Metric.Max
		} else if id == "host_mem_usage_bytes" {
			lHostResp.mem_usage_current = data.Value
			lHostResp.mem_usage_min = data.Metric.Min
			lHostResp.mem_usage_max = data.Metric.Max
		}
	}
	lHostResp.connections = n.nodeSummary.Adjacency
	return lHostResp
}

func parseServiceMetrics(n *Node) *serviceResp {
	lServiceResp := &serviceResp{
		id:        n.id,
		name:      n.label,
		timestamp: time.Now().UTC().String(),
	}

	metaData := n.nodeSummary.Metadata
	for _, data := range metaData {
		id := data.ID
		switch id {
		case "pod":
			lServiceResp.pods_count = data.Value
		case "kubernetes_namespace":
			lServiceResp.namespace = data.Value
		case "kubernetes_ports":
			lServiceResp.ports = data.Value
		}

	}
	lServiceResp.connections = n.nodeSummary.Adjacency
	return lServiceResp
}

// var serviceMapping = make(map[string]map[string][]interface{})
// var podMapping = make(map[string]map[string][]interface{})
// var containerMapping = make(map[string]map[string][]interface{})
// var processMapping = make(map[string]interface{})

func parseContainerMetrics(n *Node) *containerResp {
	lContainerResp := &containerResp{
		id:        n.id,
		uuid:      n.id,
		name:      n.label,
		timestamp: time.Now().UTC().String(),
	}

	for _, data := range n.nodeSummary.Metadata {
		id := data.ID
		switch id {
		case "docker_image_tag":
			lContainerResp.image_tag = data.Value
		case "docker_image_name":
			lContainerResp.image_name = data.Value
		case "docker_container_command":
			lContainerResp.container_command_count = data.Value
		case "docker_container_uptime":
			lContainerResp.container_uptime = data.Value
		case "docker_container_restart_count":
			lContainerResp.restart_count = data.Value
		case "docker_container_ips":
			lContainerResp.ip_addr = data.Value
		case "docker_container_created":
			lContainerResp.created = data.Value
		}
	}

	for _, data := range n.nodeSummary.Metrics {
		id := data.ID
		switch id {
		case "docker_cpu_total_usage":
			lContainerResp.cpu_usage_current = data.Value
			lContainerResp.cpu_usage_min = data.Metric.Min
			lContainerResp.cpu_usage_max = data.Metric.Max
		case "docker_memory_usage":
			lContainerResp.mem_usage_current = data.Value
			lContainerResp.mem_usage_min = data.Metric.Min
			lContainerResp.mem_usage_max = data.Metric.Max
		}
	}

	lContainerResp.connections = n.nodeSummary.Adjacency
	for _, data := range n.nodeSummary.Parents {
		topologyId := data.TopologyID
		if topologyId == "pods" {
			lContainerResp.pod_id = data.ID
			lContainerResp.pod_name = data.Label
		} else if topologyId == "hosts" {
			lContainerResp.host_id = data.ID
		}

	}
	return lContainerResp
}

func parsePodMetrics(n *Node) *podResp {
	lPodResp := &podResp{
		id:        n.id,
		uuid:      n.id,
		name:      n.label,
		timestamp: time.Now().UTC().String(),
	}
	for _, data := range n.nodeSummary.Metadata {
		id := data.ID
		switch id {
		case "kubernetes_state":
			lPodResp.State = data.Value
		case "kubernetes_ip":
			lPodResp.HostAddress = data.Value
		case "container":
			lPodResp.Containers_count = data.Value
		case "kubernetes_namespace":
			lPodResp.Namespace = data.Value
		case "kubernetes_restart_count":
			lPodResp.restart_count = data.Value
		}
	}

	for _, data := range n.nodeSummary.Metrics {
		id := data.ID
		if id == "docker_cpu_total_usage" {
			lPodResp.cpu_usage_current = data.Value
			lPodResp.cpu_usage_min = data.Metric.Min
			lPodResp.cpu_usage_max = data.Metric.Max
		} else if id == "docker_memory_usage" {
			lPodResp.mem_usage_current = data.Value
			lPodResp.mem_usage_min = data.Metric.Min
			lPodResp.mem_usage_max = data.Metric.Max
		}
	}

	lPodResp.connections = n.nodeSummary.Adjacency

	for _, data := range n.nodeSummary.Parents {
		topologyId := data.TopologyID
		if topologyId == "hosts" {
			lPodResp.host_id = data.ID
		} else if topologyId == "services" {
			lPodResp.service_id = data.ID
		}
	}
	return lPodResp
}

func parseProcessMetrics(n *Node) *processResp {
	lProcessResp := &processResp{
		id:        n.id,
		uuid:      n.id,
		name:      n.label,
		timestamp: time.Now().UTC().String(),
	}

	for _, data := range n.nodeSummary.Metadata {
		id := data.ID
		if id == "pid" {
			lProcessResp.pid = data.Value
		} else if id == "threads" {
			lProcessResp.threads_count = data.Value
		}
	}

	for _, data := range n.nodeSummary.Metrics {
		id := data.ID
		if id == "process_cpu_usage_percent" {
			lProcessResp.cpu_usage_current = data.Value
			lProcessResp.cpu_usage_min = data.Metric.Min
			lProcessResp.cpu_usage_max = data.Metric.Max
		} else if id == "process_memory_usage_bytes" {
			lProcessResp.mem_usage_current = data.Value
			lProcessResp.mem_usage_min = data.Metric.Min
			lProcessResp.mem_usage_max = data.Metric.Max
		}
	}

	for _, data := range n.nodeSummary.Parents {
		topologyId := data.TopologyID
		if topologyId == "containers" {
			lProcessResp.container_id = data.ID
		} else if topologyId == "hosts" {
			lProcessResp.host_id = data.ID
		}

	}
	return lProcessResp

}
