package appnomic

import (
	"fmt"
	"log"
	"os"
	"text/template"

	"github.com/weaveworks/scope/render/detailed"
)

type Node struct {
	id          string
	label       string
	nodeSummary detailed.NodeSummary
	parents     []*Node
	children    []*Node
	nodeType    string
}

func NewNode(id, label string, summary detailed.NodeSummary, nodeType string) *Node {
	return &Node{
		id:          id,
		label:       label,
		nodeSummary: summary,
		nodeType:    nodeType,
	}
}

func (n *Node) GetId() string {
	return n.id
}
func (n *Node) SetId(s string) {
	n.id = s
}

func (n *Node) GetLabel() string {
	return n.label
}
func (n *Node) SetLabel(s string) {
	n.label = s
}

func (n *Node) GetParents() []*Node {
	return n.parents
}
func (n *Node) AddParent(s *Node) {
	n.parents = append(n.parents, s)
	s.AddChildren(n)
}

func (n *Node) AddChildren(s ...*Node) {
	n.children = append(n.children, s...)
}

func (n *Node) GetChildren() []*Node {
	return n.children
}

func (n *Node) GetNodeSummary() detailed.NodeSummary {
	return n.nodeSummary
}
func (n *Node) SetNodeSummary(s detailed.NodeSummary) {
	n.nodeSummary = s
}

func (n *Node) GetJsonBody() string {
	templatefilename := getTemplateFilename(n.nodeType)
	templ, err := template.ParseFiles(templatefilename)
	if err != nil {
		log.Printf("Error in parsing template %s: %s", templatefilename, err.Error())
		return ""
	}
	err = templ.Execute(os.Stdout, n)
	if err != nil {
		log.Printf("Error in executing template: %s", err.Error())
		return ""
	}
	return ""
}

func walkNode(n *Node) {
	if n == nil {
		return
	}
	log.Printf("%s, %s: [ ", n.label, n.nodeType)

	for _, child := range n.GetChildren() {
		walkNode(child)
	}
	log.Print(" ] ")
}

func printNode(n *Node) {
	fmt.Printf("\n\nprinting node ===> \nid: %s, label: %s, nodetype: %s\n", n.id, n.label, n.nodeType)
	fmt.Println("Parents: ")
	for i, parent := range n.parents {
		fmt.Printf("parent %d -> id: %s, label: %s, nodetype: %s\n", i, parent.id, parent.label, parent.nodeType)
	}
	fmt.Println("children: ")
	for i, child := range n.children {
		fmt.Printf("child %d -> id: %s, label: %s, nodetype: %s\n", i, child.id, child.label, child.nodeType)
	}
}
