package appnomic

import (
	"log"
	"strings"

	"github.com/weaveworks/scope/render/detailed"
)

var Topologies = []string{"services", "pods", "containers"}
var templateDir string

const root = "root"

type Topology struct {
	nodeSummaries map[string]*Node
	RootNode      *Node
}

var gTopology *Topology
var gOldTopology *Topology

func (n *Topology) Exists(id string) bool {
	_, ok := n.nodeSummaries[id]
	return ok
}
func (n *Topology) GetNodeSummaries() map[string]*Node {
	return n.nodeSummaries
}
func (n *Topology) AddNodeSummary(summary detailed.NodeSummary) {

	if n.nodeSummaries == nil {
		n.nodeSummaries = make(map[string]*Node)
	}
	if summary.Label == "POD" {
		return
	}
	nodetype := getNodeType(summary.ID)
	if nodetype == "" {
		log.Printf("Node type for id %s not found, skipping this node", summary.ID)
		return
	}
	s := NewNode(summary.ID, summary.Label, summary, nodetype)
	log.Printf("\n\nnode added to the topology: %s", s.id)
	log.Println("node info", summary.ID, summary.Label, summary.Parents)
	n.nodeSummaries[s.id] = s

}

func (n *Topology) AddNodeSummaries(nodeSummaries detailed.NodeSummaries) {
	for _, nodesummary := range nodeSummaries {
		n.AddNodeSummary(nodesummary)
	}
}

func (n *Topology) flushDeletedSummaries() {
}

func (n *Topology) CleanUp() {
	n.nodeSummaries = make(map[string]*Node)
	n.RootNode = nil
	log.Println("Successfully cleaned up the topology")
}

func (n *Topology) MakeRelations() {
	log.Println("Making relationship")
	log.Printf("### topology: %#v\n", n)
	for _, lNode := range n.nodeSummaries {
		summary := lNode.GetNodeSummary()
		log.Println("node info", summary.ID, summary.Label, summary.Parents)
	}
	if n.RootNode == nil {
		rtnode := NewNode(root, root, detailed.NodeSummary{}, root)
		n.RootNode = rtnode
	}
	for _, lNode := range n.nodeSummaries {
		log.Println("\n\nFinding relationship for node", lNode.id)
		var lParentFound bool
		summary := lNode.GetNodeSummary()
		for _, parent := range summary.Parents {
			log.Println("checking parent", parent)
			if parentNode, ok := n.nodeSummaries[parent.ID]; ok {
				lParentFound = true
				log.Println("Parent found, adding it to ", parentNode.id)
				lNode.AddParent(parentNode)
			}
		}

		// If parent is absent, add it to root node
		log.Println("Parent not found, adding it to root")
		if !lParentFound {
			lNode.AddParent(n.RootNode)
		}
	}
}

func (n *Topology) PrintTree() {
	log.Println("Printing tree")
	walkNode(n.RootNode)
}

func getNodeType(id string) string {
	spl := strings.Split(id, ";")
	if len(spl) == 2 {
		s := spl[1]
		if len(s) > 2 && s[0] == '<' && s[len(s)-1] == '>' {
			return s[1 : len(s)-1]
		}

	}
	return ""
}

func getTemplateFilename(nodetype string) string {
	return templateDir + "/" + nodetype + ".gotmpl"
}

func (n *Topology) printTopology() {
	log.Println("Printing topology")
	for _, node := range n.nodeSummaries {
		printNode(node)
	}
}
