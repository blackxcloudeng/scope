package appnomic

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

var gExporter *Exporter

type Exporter struct {
	//AppnAuthURI   string
	AppnDbIp      string
	AppnDbPort    string
	AppnIp        string
	AppnPort      string
	AppnAccId     string
	AppnVer       string
	AppnConfigurl string
	Appnauthurl   string
	AppnUsername  string
	AppnPassword  string
	AppnClientID  string
	AppnGrantType string
	AppnUrls      *AppnUrls
	//AuthData      *AuthReqData
	AuthToken string
}

var (
	authReqData = map[string][]string{
		"username":   []string{"appsoneadmin"},
		"password":   []string{"Appsone@123"},
		"client_id":  []string{"admin-cli"},
		"grant_type": []string{"password"},
	}
)

const (
	ADD_AUTH_URI = "/auth/realms/master/protocol/openid-connect/token"
	ADD_SERVICE_URI      = "/addApplication"
	ADD_CONNECTION_URI   = "/addConnection"
	ADD_CLUSTER_URI      = "/addCluster"
	ADD_COMPINSTANCE_URI = "/addCompInstance"
)

type AppnUrls struct {
	AppnServiceUrl      string
	AppnConnectionUrl   string
	AppnClusterUrl      string
	AppnCompInstanceUrl string
}

//type AuthReqData struct {
//	Username  []string `json:"username"`
//	Password  []string `json:"password"`
//	ClientId  []string `json:"client_id"`
//	GrantType []string `json:"grant_type"`
//}

func (e *Exporter) Init() {
	e.AppnConfigurl = "https://" + e.AppnIp + ":" + e.AppnPort + "/appsone-controlcenter/" + e.AppnVer + "/api/" + e.AppnAccId
	e.Appnauthurl = "https://" + e.AppnIp + ":" + e.AppnPort + ADD_AUTH_URI
	e.AppnUrls = &AppnUrls{}
	e.AppnUrls.AppnServiceUrl = e.AppnConfigurl + ADD_SERVICE_URI
	e.AppnUrls.AppnClusterUrl = e.AppnConfigurl + ADD_CLUSTER_URI
	e.AppnUrls.AppnCompInstanceUrl = e.AppnConfigurl + ADD_COMPINSTANCE_URI
	e.AppnUrls.AppnConnectionUrl = e.AppnConfigurl + ADD_CONNECTION_URI
}

func (e *Exporter) SendRequest(epurl string, data []byte, method string, authToken interface{}) ([]byte, int) {
	var resp *http.Response
	var statusCode int
	var err error
	var bodyText []byte
	switch method {
	case http.MethodPost:
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client := &http.Client{Transport: tr}
		if authToken != nil {
			req, err := http.NewRequest("POST", epurl, bytes.NewBuffer(data))
			if err != nil {
				log.Fatal(err)
			}
			req.Header.Add("Content-Type", "application/json")
			req.Header.Add("Accept", "application/json")
			req.Header.Add("Authorization", authToken.(string))
			resp, err = client.Do(req)
		} else {
			formData := url.Values{}
			formData.Set("username", e.AppnUsername)
			formData.Set("password", e.AppnPassword)
			formData.Set("client_id", e.AppnClientID)
			formData.Set("grant_type", e.AppnGrantType)
			resp, err = client.PostForm(epurl, formData)
		}
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		bodyText, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		statusCode = resp.StatusCode
		if statusCode == 200 {
			return bodyText, 200
		}
	}
	return bodyText, statusCode
}

func (e *Exporter) GetAuthToken() string {
	var responseData = make(map[string]interface{})
	result, statusCode := e.SendRequest(e.Appnauthurl, []byte{}, "POST", nil)
	if statusCode == 200 {
		println(string(result))
		err := json.Unmarshal(result, &responseData)
		if err != nil {
			log.Fatal(err)
		}
		println(responseData)
		return responseData["access_token"].(string)
	} else {
		log.Fatal("No Auth available")
	}
	return ""
}

func (e *Exporter) SendRequestWithToken(epurl string, data []byte, method string) ([]byte, int) {
	var statusCode int
	var respBody []byte
	if e.AuthToken == "" {
		log.Println("Getting authToken")
		e.AuthToken = e.GetAuthToken()
	}
	if e.AuthToken != "" {
		//log.Printf("auth is available %s", e.AuthToken)
		respBody, statusCode = e.SendRequest(epurl, data, "POST", e.AuthToken)
		log.Printf("statuscode:%d", statusCode)
		log.Printf("respbody:%+v", string(respBody))
		if statusCode == 401 {
			log.Println("Auth Token expired... Refreshing token\n")
			return e.SendRequestWithToken(epurl, data, method)
		} else if statusCode == 200 {
			return respBody, statusCode
		}
	} else {
		log.Fatal("No valid token available to send add service request!!!!\n")
	}
	return respBody, statusCode
}

func (e *Exporter) SendToAppn(urlType string, data string) ([]byte, int) {
	var epUrl string
	switch urlType {
	case "service":
		epUrl = e.AppnUrls.AppnServiceUrl
	case "cluster":
		epUrl = e.AppnUrls.AppnClusterUrl
	case "compInstance":
		epUrl = e.AppnUrls.AppnCompInstanceUrl
	case "connection":
		epUrl = e.AppnUrls.AppnConnectionUrl
	}
	log.Printf("epUrl: %s", epUrl)
	log.Printf("Body: %s", data)
	return e.SendRequestWithToken(epUrl, []byte(data), "POST")

}

func main() {
	data, err := ioutil.ReadFile("./addApp.json")
	if err != nil {
		log.Fatal(err)
	}
	//log.Println(string(data))
	exporter := &Exporter{}
	//exporter.AppnAuthURI = "/auth/realms/master/protocol/openid-connect/token"
	exporter.AppnDbIp = "44.225.63.62 "
	exporter.AppnDbPort = "3306"
	exporter.AppnIp = "52.11.38.213"
	exporter.AppnPort = "8443"
	exporter.AppnAccId = "12345678"
	exporter.AppnVer = "v1.0"
	exporter.AppnUsername = "appsoneadmin"
	exporter.AppnPassword = "Appsone@123"
	exporter.AppnClientID = "admin-cli"
	exporter.AppnGrantType = "password"
	exporter.Init()
	respBody, isSuccess := exporter.SendRequestWithToken(exporter.AppnUrls.AppnServiceUrl, data, "POST")
	log.Println(isSuccess)
	log.Printf("sss1:%+v", string(respBody))
}

