package appnomic

import (
	"io/ioutil"
	"log"

	"github.com/weaveworks/scope/render/detailed"
)

func InitTopology() {
	gTopology = &Topology{}
	gOldTopology = &Topology{}
	// DiffInterface object to calculate the diff.
	// To use a new diffInterface, change it here.
	gDiffObj = LocalDiff{}
	// Writer interface. To update the writer change
	// here.
	gWriter = LocalRemoteWriter{}
	log.Printf("topology init: %#v", gTopology)
	templateDir = "./"
	log.Printf("connector initialisation success")
	InitExporter()
}


func InitExporter() {
	log.Printf("Exporter Init")
	gExporter = &Exporter{}
        gExporter.AppnDbIp = "44.225.63.62 "
        gExporter.AppnDbPort = "3306"
        gExporter.AppnIp = "52.11.38.213"
        gExporter.AppnPort = "8443"
        gExporter.AppnAccId = "1234567890"
        gExporter.AppnVer = "v1.0"
        gExporter.AppnUsername = "appsoneadmin"
        gExporter.AppnPassword = "Appsone@123"
        gExporter.AppnClientID = "admin-cli"
        gExporter.AppnGrantType = "password"
        gExporter.Init()
}

func AddNodeSummaries(nodeSummaries detailed.NodeSummaries) {
	gTopology.AddNodeSummaries(nodeSummaries)
}

// Process and send data caclutates the node topology. It then calculates the
// diff using diffInterface object and write the diff objects using the writer
// interface.
func ProcessAndSendData() error {
	gTopology.MakeRelations()
	// this function is to check if the topology is created correctly or not.
	gTopology.PrintTree()
	// gTopology.printTopology()

	interests := []string{service, pod, container}
	for i := range interests {
		log.Printf("processing nodetypes %s", interests[i])
		added, removed := gDiffObj.GetDiffObjects(interests[i], gTopology)
		err := gWriter.WriteObjects(interests[i], added, removed, gTopology)
		if err != nil {
			log.Printf("Error in writing nodetypes %s: %s", interests[i], err.Error())
		}
	}

/*
	lDiffNodeIds := gDiffObj.GetDiffIds(gTopology)
	for lDiffNodeId, lOperation := range lDiffNodeIds {
		if lNode, ok := gTopology.nodeSummaries[lDiffNodeId]; ok {
			err := gWriter.Write(lNode, lOperation)
			if err != nil {
				log.Printf("Error in writing node %s: %s", lNode.id, err.Error())
			}
		} else {
			log.Printf("diff id %s not in topology. it's a bug, please fix", lDiffNodeId)
		}

	}
*/

	// Cleanup now
	*gOldTopology = *gTopology
	gTopology.CleanUp()
	// log.Println("old", gOldTopology)
	// log.Println("new", gTopology)
	return nil
}

func sendToAppnomic(id string, body []byte) {
	log.Println("sending body to appnmoic")
	err := ioutil.WriteFile(id+".json", body, 7777)
	if err != nil {
		log.Println("Error in writing file", id, err.Error())
	}
}
