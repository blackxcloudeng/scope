package appnomic_test

import (
	"fmt"
	"testing"

	"github.com/weaveworks/scope/appnomic"
	"github.com/weaveworks/scope/render/detailed"
	// "github.com/bmizerany/assert"
)

func TestTopology(t *testing.T) {
	appnomic.InitTopology()
	addSvcSummaries()
	addPodSummaries()
	addContainerSummaries()
	appnomic.ProcessAndSendData()

}

func newDetailedSummary(id, label, nodetype string, parents []string, parenttype string) detailed.NodeSummary {
	summary := detailed.NodeSummary{}
	summary.ID = id
	summary.Label = label
	for _, p := range parents {
		summary.Parents = append(summary.Parents, detailed.Parent{
			ID:    getId(p, parenttype),
			Label: p,
		})
	}
	return summary
}

func genStringSlice(name string, n, m int) []string {
	result := make([]string, 0)
	for i := n; i < m; i++ {
		result = append(result, fmt.Sprintf("%s%d", name, i))
	}
	return result
}

func getId(label, labeltype string) string {
	return fmt.Sprintf("%s;<%s>", label, labeltype)
}

func addSvcSummaries() {
	services := genStringSlice("svc", 0, 3)
	svcSummaries := detailed.NodeSummaries{}
	for _, svc := range services {
		//id := fmt.Sprintf("%s;<%s>", svc, "service")
		id := getId(svc, "service")
		svcSummaries[id] = newDetailedSummary(id, svc, "service", nil, "")
	}
	appnomic.AddNodeSummaries(svcSummaries)
}

func addPodSummaries() {
	services := genStringSlice("svc", 0, 3)
	pods := genStringSlice("pod", 0, 5)
	podSummaries := detailed.NodeSummaries{}
	i := 0
	podname := pods[0]
	podSummaries[getId(podname, "pod")] = newDetailedSummary(
		getId(podname, "pod"), podname, "pod", services[i:i+1], "service",
	)
	i++
	podname = pods[1]
	podSummaries[getId(podname, "pod")] = newDetailedSummary(
		getId(podname, "pod"), podname, "pod", services[i:i+1], "service",
	)
	podname = pods[2]
	podSummaries[getId(podname, "pod")] = newDetailedSummary(
		getId(podname, "pod"), podname, "pod", services[i:i+1], "service",
	)
	i++
	podname = pods[3]
	podSummaries[getId(podname, "pod")] = newDetailedSummary(
		getId(podname, "pod"), podname, "pod", services[i:i+1], "service",
	)
	podname = pods[4]
	podSummaries[getId(podname, "pod")] = newDetailedSummary(
		getId(podname, "pod"), podname, "pod", services[i:i+1], "service",
	)
	appnomic.AddNodeSummaries(podSummaries)

}

func addContainerSummaries() {
	pods := genStringSlice("pod", 0, 5)
	containers := genStringSlice("container", 0, 5)
	containerSummaries := detailed.NodeSummaries{}
	containername := containers[0]
	i := 0
	containerSummaries[getId(containername, "container")] = newDetailedSummary(
		getId(containername, "container"), containername, "container", pods[i:i+1], "pod",
	)
	i++
	containername = containers[1]
	containerSummaries[getId(containername, "container")] = newDetailedSummary(
		getId(containername, "container"), containername, "container", pods[i:i+1], "pod",
	)
	i++
	containername = containers[2]
	containerSummaries[getId(containername, "container")] = newDetailedSummary(
		getId(containername, "container"), containername, "container", pods[i:i+1], "pod",
	)
	i++
	containername = containers[3]
	containerSummaries[getId(containername, "container")] = newDetailedSummary(
		getId(containername, "container"), containername, "container", pods[i:i+1], "pod",
	)
	i++
	containername = containers[4]
	containerSummaries[getId(containername, "container")] = newDetailedSummary(
		getId(containername, "container"), containername, "container", pods[i:i+1], "pod",
	)
	appnomic.AddNodeSummaries(containerSummaries)
}
